package com.qa.tests;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class HttpURLConnectionDemo {

	public static void main(String[] args) {
		HttpURLConnectionDemo httpURLConnectionDemo = new HttpURLConnectionDemo();
//		httpURLConnectionDemo.getDecision(new HashSet<>());
		httpURLConnectionDemo.getMergedDecision(new HashSet<>());
	}

	public Map<String, String> getDecision(Set<String> setOfCreativeIds) {
//		List<String> listOfCreativeIds = new ArrayList<>();
//		listOfCreativeIds.add("5e9059f9720ad2e74d6d2200");
//		listOfCreativeIds.add("5e6ef98794aa777768e46bbb");
//		setOfCreativeIds.addAll(listOfCreativeIds);

		/*
		 * Conversion of ArrayList into String representing the same.
		 */
		JsonArray jsonArray = new JsonArray();
		Gson gson = new Gson();
		JsonElement jsonElement = gson.toJsonTree(setOfCreativeIds);
		jsonArray = jsonElement.getAsJsonArray();
		String commaSeparated = jsonArray.toString();
		/*
		 * The converted String will look equivalent to below.
		 */
//	String commaSeparated = "[\"5e9059f9720ad2e74d6d2200\",\"5e6ef98794aa777768e46bbb\"]";

		URL targetUrl;
		String response = new String();
		Map<String, String> mapOfCreativeIdDecision = new HashMap<>();
		try {
			targetUrl = new URL("http://34.233.94.164/CreativeScan/theorem/getOnlyResponseDetailsByIds");
//		targetUrl = new URL("http://localhost:8080/theorem/getOnlyResponseDetailsByIds");

			HttpURLConnection httpConnection = (HttpURLConnection) targetUrl.openConnection();
			httpConnection.setDoOutput(true);
			httpConnection.setRequestMethod("POST");
			httpConnection.setRequestProperty("Content-Type", "application/json");
			httpConnection.setRequestProperty("authorization",
					"Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.e30.HxpXX6Say4SONtv_57MvX63CM_QIvMJvVF8de-aJhrM");

			/**
			 * Sending response to Unity
			 */
			OutputStream outputStream = httpConnection.getOutputStream();
			outputStream.write(commaSeparated.getBytes());
			outputStream.flush();

			String statusCode = String.valueOf(httpConnection.getResponseCode());
			String resMes = String.valueOf(httpConnection.getResponseMessage());
			/*
			 * Get inside if condition if the status is 200, success.
			 */
			if (statusCode.equals("200")) {
				System.out.println("statusCode = " + statusCode + ", resMes = " + resMes);
				/*
				 * Convert the response received from server, which resides inside input stream
				 * into string
				 */
				InputStream inputStream = httpConnection.getInputStream();
				byte[] inputByteArray = new byte[httpConnection.getContentLength()];
				inputStream.read(inputByteArray);
				response = new String(inputByteArray);
				System.out.println("Response = " + response);

				/*
				 * I have given the conversion of this String into Json in my another example
				 * code which I have shared previously, with the name "StringToJson".
				 */
			} else {
				System.out.println("statusCode = " + statusCode + ", resMes = " + resMes);
				InputStream errorInputStream = httpConnection.getErrorStream();
//			byte[] byteArray = new byte[httpConnection.getContentLength()];
				byte[] byteArray = new byte[2000];
				errorInputStream.read(byteArray);
				response = new String(byteArray);
				System.out.println("Response = " + response);
			}

			@SuppressWarnings("deprecation")
			JsonArray responseJsonArray = new JsonParser().parse(response).getAsJsonArray();

			for (int i = 0; i < responseJsonArray.size(); i++) {
				JsonObject jsonObject = responseJsonArray.get(i).getAsJsonObject();
				JsonArray projectsArray = jsonObject.get("response").getAsJsonObject().get("projects").getAsJsonArray();

				for (int projectsCount = 0; projectsCount < projectsArray.size(); projectsCount++) {
					JsonObject programmaticJsonResponse = projectsArray.get(projectsCount).getAsJsonObject();
					String creativeId = programmaticJsonResponse.get("creativeId").getAsString();
					System.out.println(creativeId);
					String decision = programmaticJsonResponse.get("scanDecision").getAsString();
					System.out.println(decision);
					mapOfCreativeIdDecision.put(creativeId, decision);
				}
			}
			System.out.println(mapOfCreativeIdDecision);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return mapOfCreativeIdDecision;
	}

	public Map<String, String> getMergedDecision(Set<String> setOfCreativeIds) {

//		List<String> listOfCreativeIds = new ArrayList<>();
//		listOfCreativeIds.add("5e9059f9720ad2e74d6d2200");
//		listOfCreativeIds.add("5e6ef98794aa777768e46bbb");
//		setOfCreativeIds.addAll(listOfCreativeIds);
		Map<String, String> mapOfCreativeIdDecision = new HashMap<>();
		for (String creativeId1 : setOfCreativeIds) {
			/*
			 * Conversion of ArrayList into String representing the same.
			 */
			JsonArray jsonArray = new JsonArray();
			Gson gson = new Gson();
			JsonElement jsonElement = gson.toJsonTree(creativeId1);
			jsonArray.add(jsonElement);
			String commaSeparated = jsonArray.toString();
			/*
			 * The converted String will look equivalent to below.
			 */
//	String commaSeparated = "[\"5e9059f9720ad2e74d6d2200\",\"5e6ef98794aa777768e46bbb\"]";

			URL targetUrl;
			String response = new String();

			try {
				targetUrl = new URL("http://34.233.94.164/CreativeScan/theorem/getOnlyResponseDetailsByIds");
//		targetUrl = new URL("http://localhost:8080/theorem/getOnlyResponseDetailsByIds");

				HttpURLConnection httpConnection = (HttpURLConnection) targetUrl.openConnection();
				httpConnection.setDoOutput(true);
				httpConnection.setRequestMethod("POST");
				httpConnection.setRequestProperty("Content-Type", "application/json");
				httpConnection.setRequestProperty("authorization",
						"Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.e30.HxpXX6Say4SONtv_57MvX63CM_QIvMJvVF8de-aJhrM");

				/**
				 * Sending response to Unity
				 */
				OutputStream outputStream = httpConnection.getOutputStream();
				outputStream.write(commaSeparated.getBytes());
				outputStream.flush();

				String statusCode = String.valueOf(httpConnection.getResponseCode());
				String resMes = String.valueOf(httpConnection.getResponseMessage());
				/*
				 * Get inside if condition if the status is 200, success.
				 */
				if (statusCode.equals("200")) {
					System.out.println("statusCode = " + statusCode + ", resMes = " + resMes);
					/*
					 * Convert the response received from server, which resides inside input stream
					 * into string
					 */
					InputStream inputStream = httpConnection.getInputStream();
					byte[] inputByteArray = new byte[httpConnection.getContentLength()];
					inputStream.read(inputByteArray);
					response = new String(inputByteArray);
					System.out.println("Response = " + response);

					/*
					 * I have given the conversion of this String into Json in my another example
					 * code which I have shared previously, with the name "StringToJson".
					 */
				} else {
					System.out.println("statusCode = " + statusCode + ", resMes = " + resMes);
					InputStream errorInputStream = httpConnection.getErrorStream();
//			byte[] byteArray = new byte[httpConnection.getContentLength()];
					byte[] byteArray = new byte[2000];
					errorInputStream.read(byteArray);
					response = new String(byteArray);
					System.out.println("Response = " + response);
				}

				@SuppressWarnings("deprecation")
				JsonArray responseJsonArray = new JsonParser().parse(response).getAsJsonArray();

				for (int i = 0; i < responseJsonArray.size(); i++) {
					JsonObject jsonObject = responseJsonArray.get(i).getAsJsonObject();
					JsonArray projectsArray = jsonObject.get("response").getAsJsonObject().get("projects")
							.getAsJsonArray();

					for (int projectsCount = 0; projectsCount < projectsArray.size(); projectsCount++) {
						JsonObject programmaticJsonResponse = projectsArray.get(projectsCount).getAsJsonObject();
						String creativeId = programmaticJsonResponse.get("creativeId").getAsString();
						System.out.println(creativeId);
						String decision = programmaticJsonResponse.get("scanDecision").getAsString();
						System.out.println(decision);
						mapOfCreativeIdDecision.put(creativeId, decision);
					}
				}
				System.out.println(mapOfCreativeIdDecision);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return mapOfCreativeIdDecision;
	}
}
