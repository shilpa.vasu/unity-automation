package com.qa.tests;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.qa.base.TestBase;
import com.qa.client.RestClient;
import com.qa.data.Xls_Reader;
import io.github.bonigarcia.wdm.WebDriverManager;

public class PostAPITest extends TestBase {
	TestBase testBase;
	String serviceUrl;
	String apiUrl;
	String url;
	RestClient restClient;
	CloseableHttpResponse closebaleHttpResponse;
	WebDriver driver;
	String indexCreativeIdRowMap;
	String creativeID;

	@Test(priority = 1)
	public void setUp() throws ClientProtocolException, IOException, InterruptedException {
		
		WebDriverManager.firefoxdriver().setup();
		//WebDriverManager.chromedriver().setup();
		WebDriver driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().deleteAllCookies();
		driver.get("http://34.225.173.157/Unity/scp/index.php");
		driver.findElement(By.id("name")).sendKeys("shilpa.vasu");
		driver.findElement(By.name("passwd")).sendKeys("shilpa_123");
		driver.findElement(By.name("submit")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath("//a[@title='Manual Tickets']")).click();
		Thread.sleep(8000);

		// Reading the excel data
		Xls_Reader checksExcel = new Xls_Reader(
				"F:\\Shilpa\\2020\\Workspace\\UnityFrameWorkDemo\\src\\main\\java\\com\\qa\\data\\New Check May21.xlsx");
		String cheksSheetName = "Android Checks";
		Thread.sleep(2000);

		HashMap<Integer, String> indexManualCodeMap = new HashMap<>();
		HashMap<String, Integer> indexCreativeIdRowMap = new HashMap<>();
		int index = 0;
		int totalColsCount = checksExcel.getColumnCount(cheksSheetName);
		int totalRowsCount = checksExcel.getRowCount(cheksSheetName);
		int indexCount = 0;
		int creativeCount = 2;

		// Row count should be start from 2, it will loop the creative ID until total
		// rows count equals to row count

		for (int rowCount = 2; rowCount <= totalRowsCount; rowCount++) {
			// It will iterate through checks list
			for (int colCount = 0; colCount < totalColsCount; colCount++) {

				// Due to check code first row of creative field is blank, handled with empty
				// string
				// String creativeID = "";
				String data = "";

				// when column count is zero, then the creative ID will fetch from sheet
				if (colCount == 0) {
					creativeID = checksExcel.getCellData(cheksSheetName, colCount, rowCount);
					/*
					 * To handle blank cell, skipping that cell if it's in 2nd row
					 */
					if (rowCount == 2) {
						indexCount++;
						continue;
					}
					indexCreativeIdRowMap.put(creativeID, ++creativeCount);
					// row count is greater than 3, then it will search the creative ID
					if (rowCount >= 3) {
						System.out.println(creativeID);
						
//						if(rowCount%22==0)
//						{
//							Thread.sleep(6000);
//						}
						
//						if(rowCount%2==0)
//						{
//							driver.close();
//							WebDriver driver1 = new ChromeDriver();
//							driver1.get("http://34.225.173.157/Unity/scp/index.php");
//							driver1.manage().window().maximize();
//							driver1.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
//							driver1.findElement(By.id("name")).sendKeys("shilpa.vasu");
//							driver1.findElement(By.name("passwd")).sendKeys("shilpa_123");
//							driver1.findElement(By.name("submit")).click();
//							Thread.sleep(5000);
//							driver1.findElement(By.xpath("//a[@title='Manual Tickets']")).click();
//							Thread.sleep(8000);
//						}
						
						Thread.sleep(2000);
						driver.findElement(By.id("basic-ticket-search")).sendKeys(creativeID);
						Thread.sleep(2000);
						driver.findElement(By.xpath("(//input[@value='Search'])[1]")).click();
						Thread.sleep(6000);
						driver.findElement(By.xpath("//a[@title='Preview Ticket']")).click();
						Thread.sleep(4000);
						continue;
					}

				} else if (colCount > 0) {
					data = checksExcel.getCellData(cheksSheetName, colCount, rowCount);
				}
				if (rowCount == 2) {
					// Use put or add method
					if (data.contains("MT")) {
						indexManualCodeMap.put(indexCount, data);
						indexCount++;
					}
					continue;
				}
				System.out.println("Data =" + data);
				String currentCheckCode = indexManualCodeMap.get(colCount);
				if (currentCheckCode != null) {

					String xpathOfDecisionRadioButton = "decision" + currentCheckCode;
					String xpathOfCommentBox = "comment" + currentCheckCode;
					// Insert Go related xpath along with xpathOfDecisionRadioButton.

					String check = "No-Go";
					if (data.equalsIgnoreCase(check)) {
						driver.findElement(By
								.xpath("//input[@name='" + xpathOfDecisionRadioButton + "' and @value='" + data + "']"))
								.click();
//					Thread.sleep(2000);
//					driver.findElement(By.xpath("//textarea[@name='" + xpathOfCommentBox + "']")).sendKeys("TEST TEST");
						Thread.sleep(2000);
					}
				}
				if (colCount == totalColsCount - 1) {
					driver.findElement(By.xpath(
							"//td[contains(text(),'Assign Next Open Ticket : ')]/following-sibling::td//input[@value='No']"))
							.click();
					Thread.sleep(2000);
					driver.findElement(By.xpath("(//input[@value='Submit'])[2]")).click();

					String actualMessage = driver.findElement(By.xpath("(//div[@id='msg_notice'])[1]")).getText();
					String expectedMessage = "Manual Check is submitted successfully";

					if (actualMessage.equalsIgnoreCase(expectedMessage)) {
						System.out.println("Successfully created the ticket");
					} else {
						System.out.println("Not able to create a ticket");
					}
					Thread.sleep(5000);
					driver.findElement(By.xpath("//a[@title='Manual Tickets']")).click();
					Thread.sleep(8000);
					
				}
			}
		}

		Thread.sleep(5000);
		Map<String, String> mapOfCreativeIdDecision = new HashMap<>();
		HttpURLConnectionDemo httpURLConnectionDemoObj = new HttpURLConnectionDemo();
		mapOfCreativeIdDecision = httpURLConnectionDemoObj.getMergedDecision(indexCreativeIdRowMap.keySet());
		// Map<String, String> mapOfCreativeIdDecision =
		// httpURLConnectionDemoObj.getDecisionForCreativeIds("http://34.233.94.164/CreativeScan/theorem/getOnlyResponseDetailsByIds");
		HashMap<String, Integer> output = indexCreativeIdRowMap;
		System.out.println("indexCreativeIdRowMap = " + indexCreativeIdRowMap);
		checksExcel.addColumn(cheksSheetName, "Expected Decision");
		checksExcel.addColumn(cheksSheetName, "Result");
		Set<String> setOfCreativeIDs = indexCreativeIdRowMap.keySet();
		List<String> listOfCreativeIDs = new ArrayList<>();
		listOfCreativeIDs.addAll(setOfCreativeIDs);

		for (String creativeID : listOfCreativeIDs) {
			int creativeRow = indexCreativeIdRowMap.get(creativeID);
			String expectedDecision = mapOfCreativeIdDecision.get(creativeID);
			
			System.out.printf("expectedDecision =", expectedDecision);

			checksExcel.setCellData(cheksSheetName, "Expected Decision", creativeRow, expectedDecision);
			String actualDecision = checksExcel.getCellData(cheksSheetName, "Actual Decision", creativeRow);
			System.out.printf("actualDecision =", actualDecision);
			if (actualDecision.equalsIgnoreCase(expectedDecision)) {

				checksExcel.setCellData(cheksSheetName, "Result", creativeRow, "Pass");
			} else {
				checksExcel.setCellData(cheksSheetName, "Result", creativeRow, "Fail");
			}
			Thread.sleep(2000);
		}
	}
}
